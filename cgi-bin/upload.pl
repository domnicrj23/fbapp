#!/usr/bin/perl -w

use strict;
use CGI;
use CGI::Carp qw ( fatalsToBrowser );
use File::Basename;

$CGI::POST_MAX = 1024 * 1000*1;
my $safe_filename_characters = "a-zA-Z0-9_.-";
my $appdir="../fbapp";
my $upload_dir = "$appdir/upload";
my $renamedFilename="edit.jpg";
my $username="ashishranjan";
my $cgi=".";
my $query = new CGI;
print $query->header();
my $filename = $query->param("photo");
my $email_address = $query->param("email_address");
my $option=$query->param("option");
#print $option;
if ( !$filename )
{
    
    print "There was a problem uploading your photo (try a smaller file).";
    print qq|<button type="button" onclick="window.location.href='/fbapp/index.html'">back</button>|;
    exit;
}

my ( $name, $path, $extension ) = fileparse ( $filename, '..*' );
#inserting a new code to obtain extension as fileparse couldn't obtain find extension in all cases
my $ext= $filename =~ /(\.[^.]+)$/;
if(!(lc($ext) eq 'jpg'||'jpeg')){
    
    print "Extension is not jpg<br\>";
    print "Extension is :".$extension;
    print "name= ".$name." path= ".$path;
    print qq|<button type="button" onclick="window.location.href='/fbapp/index.html'">back</button>|;
    exit;
}
$filename = $name . $extension;
$filename =~ tr/ /_/;
$filename =~ s/[^$safe_filename_characters]//g;

if ( $filename =~ /^([$safe_filename_characters]+)$/ )
{
    $filename = $1;
}
else
{
    die "Filename contains invalid characters";
}

my $upload_filehandle = $query->upload("photo");
open ( UPLOADFILE, ">$upload_dir/$renamedFilename" ) or die "$!";
binmode UPLOADFILE;

while ( <$upload_filehandle> )
{
    print UPLOADFILE;
}

close UPLOADFILE;

$renamedFilename="edit.png";
#deleteing if file exits
if(-e "$upload_dir/$renamedFilename"){
    #system("rm $upload_dir/$renamedFilename");
    unlink "$upload_dir/$renamedFilename";
}
my $pathToScript="/Users/$username/Documents/Adobe\\ Scripts/$option.jsx";
my $command="perl $cgi/launcher.pl \"$pathToScript\"";
#print "command: $command <br\>";
system($command);
#print system($command). "<br\>";
#print (-e "$upload_dir/$renamedFilename")."<br\>";
my $counter=0;
while((!(-e "$upload_dir/$renamedFilename")&&($counter<60))){
    print "waiting another 1000ms<br\>";
    sleep(1);
    $counter++;
}
if($counter eq 60){
    print "there was an error launching application. Time out"."</br>";
    print qq|<button type="button" onclick="window.location.href='/fbapp/index.html'">back</button>|;
    exit;
}

print <<END_HTML;
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Thanks!</title>
<style type="text/css">
img {border: none;}
</style>
</head>
<body>
<p>Thanks for uploading your photo!</p>
<p>Your email address: $email_address</p>
<p><a href="$appdir/pic.html">Your photo</a></p>
<button type="button" onclick="window.location.href='/fbapp/index.html'">back</button>
</body>
</html>
END_HTML
